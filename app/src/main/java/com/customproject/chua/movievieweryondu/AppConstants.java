package com.customproject.chua.movievieweryondu;

public final class AppConstants {

    public static final String base_url = "http://ec2-52-76-75-52.ap-southeast-1.compute.amazonaws.com";
    public static final String movieJson = base_url + "/movie.json";
    public static final String scheduleJson = base_url + "/schedule.json";
    public static final String seatmapJson = base_url + "/seatmap.json";
    public final static String THEATER = "THEATER";

}
