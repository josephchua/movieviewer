package com.customproject.chua.movievieweryondu.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.inject.Inject;

public class Movie {

    private String movie_id;
    private String canonical_title;
    private String genre;
    private String advisory_rating;
    private String runtime_mins;
    private String release_date;
    private String synopsis;
    private String poster;
    private String poster_landscape;
    private ArrayList<String> cast;
    private String theater;

    public String getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(String movie_id) {
        this.movie_id = movie_id;
    }

    public String getAdvisory_rating() {
        return advisory_rating;
    }

    public void setAdvisory_rating(String advisory_rating) {
        this.advisory_rating = advisory_rating;
    }

    public String getCanonical_title() {
        return canonical_title;
    }

    public void setCanonical_title(String canonical_title) {
        this.canonical_title = canonical_title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getRuntime_mins() {

        double t = Double.parseDouble(runtime_mins);

        double hours = t / 60;
        double minutes = t % 60;

        String hr = "hr ";
        if(hours > 1) {
            hr = "hrs ";
        }

        String min = "min";
        if(minutes > 1) {
            min = "mins ";
        }

        return String.valueOf((int) hours) + hr
                + String.valueOf((int) minutes) + min;

    }

    public void setRuntime_mins(String runtime_mins) {
        this.runtime_mins = runtime_mins;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getPoster_landscape() {
        return poster_landscape;
    }

    public void setPoster_landscape(String poster_landscape) {
        this.poster_landscape = poster_landscape;
    }

    public String getRelease_date() {

        SimpleDateFormat initialFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat returnFormat = new SimpleDateFormat("MMM dd, yyyy");
        Date date = new Date();
        try {
            date = initialFormat.parse(release_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return returnFormat.format(date);
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getCast() {
        return Arrays.toString(cast.toArray())
                .replaceAll("\\[", "")
                .replaceAll("\\]", "");
    }

    public void setCast(ArrayList<String> cast) {
        this.cast = cast;
    }

    public String getTheater() {
        return theater;
    }

    public void setTheater(String theater) {
        this.theater = theater;
    }
}
