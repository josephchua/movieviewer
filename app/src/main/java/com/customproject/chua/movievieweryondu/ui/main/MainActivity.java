package com.customproject.chua.movievieweryondu.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.androidnetworking.widget.ANImageView;
import com.customproject.chua.movievieweryondu.AppConstants;
import com.customproject.chua.movievieweryondu.R;
import com.customproject.chua.movievieweryondu.model.Movie;
import com.customproject.chua.movievieweryondu.ui.base.BaseActivity;
import com.customproject.chua.movievieweryondu.ui.seatmap.SeatMapActivity;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class  MainActivity extends BaseActivity implements MainMvpView {


    @BindView(R.id.banner)
    ANImageView banner;

    @BindView(R.id.banner2)
    ANImageView banner2;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.genre)
    TextView genre;

    @BindView(R.id.advisory_rating)
    TextView advisory_rating;

    @BindView(R.id.release_date)
    TextView release_date;

    @BindView(R.id.duration)
    TextView duration;

    @BindView(R.id.synopsis)
    TextView synopsis;

    @BindView(R.id.cast)
    TextView cast;

    @BindView(R.id.seatmap)
    Button seatmap;

    @Inject
    MainMvpPresenter<MainMvpView> mainMvpViewMainMvpPresenter;

    String theater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);

        ButterKnife.bind(this);

        mainMvpViewMainMvpPresenter.onAttach(this);

    }

    @OnClick(R.id.seatmap)
    public void onSeatMapClicked() {
        mainMvpViewMainMvpPresenter.buttonClicked();
    }

    @Override
    public void display(Movie movie) {
        name.setText(movie.getCanonical_title());
        genre.setText(movie.getGenre());
        advisory_rating.setText(movie.getAdvisory_rating());
        duration.setText(movie.getRuntime_mins());
        release_date.setText(movie.getRelease_date());
        synopsis.setText(movie.getSynopsis());
        cast.setText(movie.getCast());

        banner.setImageUrl(movie.getPoster_landscape());
        banner2.setImageUrl(movie.getPoster());

        theater = movie.getTheater();
    }

    @Override
    public void nextActivity() {
        startActivity(new Intent(this, SeatMapActivity.class)
                .putExtra(AppConstants.THEATER, theater));
    }
}
