package com.customproject.chua.movievieweryondu.ui.main;

import com.customproject.chua.movievieweryondu.ui.base.MvpView;
import com.customproject.chua.movievieweryondu.model.Movie;

public interface MainMvpView extends MvpView {

    void display(Movie movie);

    void nextActivity();

}
