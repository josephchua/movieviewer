package com.customproject.chua.movievieweryondu.ui.base;

public interface MvpView {

    void showLoading();

    void hideLoading();

    void onError(String message);

    boolean isNetworkConnected();

}
