package com.customproject.chua.movievieweryondu.model;

import java.util.ArrayList;

public class Seatmap {

    ArrayList<ArrayList<String>> seatmap;
    Available available;

    public ArrayList<ArrayList<String>> getSeatmap() {
        return seatmap;
    }

    public void setSeatmap(ArrayList<ArrayList<String>> seatmap) {
        this.seatmap = seatmap;
    }

    public Available getAvailable() {
        return available;
    }

    public void setAvailable(Available available) {
        this.available = available;
    }
}
