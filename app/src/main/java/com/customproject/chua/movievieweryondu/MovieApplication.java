package com.customproject.chua.movievieweryondu;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.customproject.chua.movievieweryondu.di.component.ApplicationComponent;
import com.customproject.chua.movievieweryondu.di.component.DaggerApplicationComponent;
import com.customproject.chua.movievieweryondu.di.module.ApplicationModule;

public class MovieApplication extends Application{

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        applicationComponent.inject(this);

        AndroidNetworking.initialize(getApplicationContext());

    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
