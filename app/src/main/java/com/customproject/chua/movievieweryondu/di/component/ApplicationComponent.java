package com.customproject.chua.movievieweryondu.di.component;

import android.app.Application;
import android.content.Context;

import com.customproject.chua.movievieweryondu.MovieApplication;
import com.customproject.chua.movievieweryondu.data.DataManager;
import com.customproject.chua.movievieweryondu.di.ApplicationContext;
import com.customproject.chua.movievieweryondu.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject (MovieApplication movieApplication);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();

}
