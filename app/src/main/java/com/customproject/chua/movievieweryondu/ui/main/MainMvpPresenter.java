package com.customproject.chua.movievieweryondu.ui.main;

import com.customproject.chua.movievieweryondu.ui.base.MvpPresenter;
import com.customproject.chua.movievieweryondu.di.PerActivity;

@PerActivity
public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {

    void fetchData();

    void buttonClicked();

}
