package com.customproject.chua.movievieweryondu.ui.main;

import android.util.Log;

import com.customproject.chua.movievieweryondu.ui.base.BasePresenter;
import com.customproject.chua.movievieweryondu.data.DataManager;
import com.customproject.chua.movievieweryondu.model.Movie;


import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements MainMvpPresenter<V> {

    @Inject
    public MainPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
        fetchData();
    }

    @Override
    public void fetchData() {
        getCompositeDisposable().add(getDataManager().callMovieApi()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<Movie>() {
            @Override
            public void accept(Movie movie) throws Exception {
                getMvpView().display(movie);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Log.e(MainPresenter.class.getSimpleName(), throwable.getMessage());
            }
        }));
    }

    @Override
    public void buttonClicked() {
        getMvpView().nextActivity();
    }
}
