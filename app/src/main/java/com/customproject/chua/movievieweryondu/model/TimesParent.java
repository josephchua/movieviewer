package com.customproject.chua.movievieweryondu.model;

import java.util.ArrayList;

public class TimesParent {

    private String parent;
    private ArrayList<TimesChild> times;

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public ArrayList<TimesChild> getTimes() {
        return times;
    }

    public void setTimes(ArrayList<TimesChild> times) {
        this.times = times;
    }
}
