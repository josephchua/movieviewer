package com.customproject.chua.movievieweryondu.data;

import com.customproject.chua.movievieweryondu.data.network.ApiHelper;
import com.customproject.chua.movievieweryondu.model.Movie;
import com.customproject.chua.movievieweryondu.model.Schedule;
import com.customproject.chua.movievieweryondu.model.Seatmap;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppDataManager implements DataManager {

    private ApiHelper apiHelper;

    @Inject
    public AppDataManager(ApiHelper apiHelper) {
        this.apiHelper = apiHelper;
    }

    @Override
    public Single<Movie> callMovieApi() {
        return apiHelper.callMovieApi();
    }

    @Override
    public Single<Schedule> callScheduleApi() {
        return apiHelper.callScheduleApi();
    }

    @Override
    public Single<Seatmap> callSeatmapApi() {
        return apiHelper.callSeatmapApi();
    }
}
