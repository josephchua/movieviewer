package com.customproject.chua.movievieweryondu.model;

import java.util.ArrayList;

public class Available {

    ArrayList<String> seats;
    String seat_count;

    public ArrayList<String> getSeats() {
        return seats;
    }

    public void setSeats(ArrayList<String> seats) {
        this.seats = seats;
    }

    public String getSeat_count() {
        return seat_count;
    }

    public void setSeat_count(String seat_count) {
        this.seat_count = seat_count;
    }
}
