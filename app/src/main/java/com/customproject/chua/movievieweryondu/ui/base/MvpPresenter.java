package com.customproject.chua.movievieweryondu.ui.base;

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);
    void onDettach();

}
