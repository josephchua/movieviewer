package com.customproject.chua.movievieweryondu.data.network;

import com.customproject.chua.movievieweryondu.model.Movie;
import com.customproject.chua.movievieweryondu.model.Schedule;
import com.customproject.chua.movievieweryondu.model.Seatmap;

import io.reactivex.Single;

public interface ApiHelper {

    Single<Movie> callMovieApi();

    Single<Schedule> callScheduleApi();

    Single<Seatmap> callSeatmapApi();

}
