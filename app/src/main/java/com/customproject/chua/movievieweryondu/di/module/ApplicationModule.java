package com.customproject.chua.movievieweryondu.di.module;

import android.app.Application;
import android.content.Context;

import com.customproject.chua.movievieweryondu.data.AppDataManager;
import com.customproject.chua.movievieweryondu.data.DataManager;
import com.customproject.chua.movievieweryondu.data.network.ApiHelper;
import com.customproject.chua.movievieweryondu.data.network.AppApiHelper;
import com.customproject.chua.movievieweryondu.di.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() { return application; }

    @Provides
    Application provideApplication() { return application; }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }


}
