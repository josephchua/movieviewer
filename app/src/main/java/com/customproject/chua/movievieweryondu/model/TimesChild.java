package com.customproject.chua.movievieweryondu.model;

public class TimesChild {

    private String id;
    private String label;
    private String schedule_id;
    private String popcorn_price;
    private String popcorn_label;
    private String seating_type;
    private String price;
    private String variant;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(String schedule_id) {
        this.schedule_id = schedule_id;
    }

    public String getPopcorn_price() {
        return popcorn_price;
    }

    public void setPopcorn_price(String popcorn_price) {
        this.popcorn_price = popcorn_price;
    }

    public String getPopcorn_label() {
        return popcorn_label;
    }

    public void setPopcorn_label(String popcorn_label) {
        this.popcorn_label = popcorn_label;
    }

    public String getSeating_type() {
        return seating_type;
    }

    public void setSeating_type(String seating_type) {
        this.seating_type = seating_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }
}
