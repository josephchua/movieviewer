package com.customproject.chua.movievieweryondu.ui.seatmap;

import com.customproject.chua.movievieweryondu.model.Schedule;
import com.customproject.chua.movievieweryondu.model.Seatmap;
import com.customproject.chua.movievieweryondu.ui.base.MvpView;

import java.util.ArrayList;

public interface SeatMapMvpView extends MvpView{

    void showTheater();

    void showScheduleDates(ArrayList<String> scheduleDatesArray, Schedule schedule);

    void showScheduleCinemas(ArrayList<String> scheduleCinemasArray);

    void showScheduleTimes(ArrayList<String> scheduleTimesArray);

    void display(Seatmap seatmap);

    void displayPrice();

    void displaySelectedSeats();


}
