package com.customproject.chua.movievieweryondu.model;

import java.util.ArrayList;

public class Schedule {

    ArrayList<Dates> dates;
    ArrayList<CinemasParent> cinemas;
    ArrayList<TimesParent> times;

    public ArrayList<Dates> getDates() {
        return dates;
    }

    public void setDates(ArrayList<Dates> dates) {
        this.dates = dates;
    }

    public ArrayList<CinemasParent> getCinemas() {
        return cinemas;
    }

    public void setCinemas(ArrayList<CinemasParent> cinemas) {
        this.cinemas = cinemas;
    }

    public ArrayList<TimesParent> getTimes() {
        return times;
    }

    public void setTimes(ArrayList<TimesParent> times) {
        this.times = times;
    }
}
