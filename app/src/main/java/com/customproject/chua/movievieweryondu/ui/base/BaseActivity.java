package com.customproject.chua.movievieweryondu.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.customproject.chua.movievieweryondu.MovieApplication;
import com.customproject.chua.movievieweryondu.di.component.ActivityComponent;
import com.customproject.chua.movievieweryondu.di.component.DaggerActivityComponent;
import com.customproject.chua.movievieweryondu.di.module.ActivityModule;

import butterknife.Unbinder;

public class BaseActivity extends AppCompatActivity implements MvpView {

    ActivityComponent activityComponent;

    private Unbinder mUnBinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((MovieApplication) getApplication()).getApplicationComponent())
                .build();
    }

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    @Override
    public void onError(String message) {
    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }

    public void setmUnBinder(Unbinder mUnBinder) {
        this.mUnBinder = mUnBinder;
    }

    @Override
    protected void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        super.onDestroy();
    }
}
