package com.customproject.chua.movievieweryondu.ui.seatmap;

import android.content.Context;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.customproject.chua.movievieweryondu.AppConstants;
import com.customproject.chua.movievieweryondu.R;
import com.customproject.chua.movievieweryondu.model.CinemasChild;
import com.customproject.chua.movievieweryondu.model.Dates;
import com.customproject.chua.movievieweryondu.model.Schedule;
import com.customproject.chua.movievieweryondu.model.Seat;
import com.customproject.chua.movievieweryondu.model.Seatmap;
import com.customproject.chua.movievieweryondu.model.TimesChild;
import com.customproject.chua.movievieweryondu.ui.base.BaseActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;

public class SeatMapActivity extends BaseActivity implements SeatMapMvpView, Seat.SeatOnClickListener {

    @BindView(R.id.textview)
    TextView textview;

    @BindView(R.id.theater)
    TextView theater;

    @BindView(R.id.dates)
    Spinner dates;

    @BindView(R.id.cinemas)
    Spinner cinemas;

    @BindView(R.id.times)
    Spinner times;

    @BindView(R.id.seats_y)
    LinearLayout seats_y;

    @BindView(R.id.sv_vertical)
    ScrollView sv_vertical;

    @BindView(R.id.sv_horizontal)
    HorizontalScrollView sv_horizontal;

    @BindView(R.id.l_selected_seats)
    LinearLayout l_selected_seats;

    @BindView(R.id.price)
    TextView price;

    @Inject
    SeatMapMvpPresenter<SeatMapMvpView> presenter;

    Schedule schedule;
    Dates selectedDate;
    CinemasChild selectedCinemasChild;
    TimesChild selectedTimesChild;

    ArrayList<Seat> seats = new ArrayList<>();
    ArrayList<String> selectedSeats = new ArrayList<>();

    private float mScale = 1.f;
    private ScaleGestureDetector mScaleDetector;
    GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seat_map_acitivity);

        ButterKnife.bind(this);

        getActivityComponent().inject(this);

        presenter.onAttach(this);

        gestureDetector = new GestureDetector(this, new GestureListener());

        mScaleDetector = new ScaleGestureDetector(this, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                float scale = 1 - detector.getScaleFactor();

                float prevScale = mScale;
                mScale += scale;

                if (mScale < 0.3f)
                    mScale = 0.3f;

                if (mScale > 1f)
                    mScale = 1f;

                ScaleAnimation scaleAnimation = new ScaleAnimation(1f / prevScale, 1f / mScale, 1f / prevScale, 1f / mScale, detector.getFocusX(), detector.getFocusY());

                scaleAnimation.setDuration(0);
                scaleAnimation.setFillAfter(true);

                seats_y.startAnimation(scaleAnimation);
                return true;
            }
        });

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        super.dispatchTouchEvent(event);
        mScaleDetector.onTouchEvent(event);
        gestureDetector.onTouchEvent(event);
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public void showTheater() {
        theater.setText(getIntent().getExtras().getString(AppConstants.THEATER));
    }

    @Override
    public void showScheduleDates(ArrayList<String> scheduleDatesArray, Schedule schedule) {

        this.schedule = schedule;

        ArrayAdapter<String> scheduleDatesAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                scheduleDatesArray);

        dates.setAdapter(scheduleDatesAdapter);
    }

    @OnItemSelected(R.id.dates)
    public void datesItemSelected(int selectedDatePosition) {

        times.setAdapter(null);
        selectedDate = schedule.getDates().get(selectedDatePosition);
        showScheduleCinemas(presenter.setScheduleCinemas(selectedDate, schedule.getCinemas()));
        displayPrice();

    }

    @Override
    public void showScheduleCinemas(ArrayList<String> scheduleCinemasArray) {

        ArrayAdapter<String> scheduleCinemasChildAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                scheduleCinemasArray);

        cinemas.setAdapter(scheduleCinemasChildAdapter);

    }

    @OnItemSelected(R.id.cinemas)
    public void cinemasChildItemSelected(int selectedCinemasChildPosition) {

        selectedCinemasChild = presenter.getSelectedCinema(selectedDate, schedule.getCinemas(), selectedCinemasChildPosition);
        showScheduleTimes(presenter.setScheduleTime(selectedCinemasChild, schedule.getTimes()));
        displayPrice();

    }

    @Override
    public void showScheduleTimes(ArrayList<String> scheduleTimesArray) {

        ArrayAdapter<String> scheduleTimesChildAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                scheduleTimesArray);

        times.setAdapter(scheduleTimesChildAdapter);

    }

    @OnItemSelected(R.id.times)
    public void timesChildItemSelected(int selectedTimesChildPosition) {

        selectedTimesChild = presenter.getSelectedTime(selectedCinemasChild, schedule.getTimes(), selectedTimesChildPosition);
        displayPrice();

    }

    @Override
    public void display(Seatmap seatmap) {

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup seatLayout = findViewById(R.id.seats_y);
        seats = presenter.createSeatView(inflater, seatLayout, seatmap, this, this);

    }

    @Override
    public void seatClickedListener(String seat_id, String status, Boolean isChecked) {

        selectedSeats = presenter.getSelectedSeats(selectedSeats, seat_id, isChecked, seats);
        displayPrice();
        displaySelectedSeats();

    }

    @Override
    public void displayPrice() {
        price.setText(presenter.computePrice(selectedTimesChild, selectedSeats));
    }

    @Override
    public void displaySelectedSeats() {

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup selectedSeatsViewGroup = findViewById(R.id.l_selected_seats);
        presenter.groupSelectedSeats(inflater, selectedSeatsViewGroup, selectedSeats);

    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            return true;
        }
    }
}
