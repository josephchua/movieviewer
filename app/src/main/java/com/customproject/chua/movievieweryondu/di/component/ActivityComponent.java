package com.customproject.chua.movievieweryondu.di.component;


import com.customproject.chua.movievieweryondu.ui.main.MainActivity;
import com.customproject.chua.movievieweryondu.di.PerActivity;
import com.customproject.chua.movievieweryondu.di.module.ActivityModule;
import com.customproject.chua.movievieweryondu.ui.seatmap.SeatMapActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject (MainActivity mainActivity);
    void inject (SeatMapActivity seatMapActivity);

}
