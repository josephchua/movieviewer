package com.customproject.chua.movievieweryondu.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.customproject.chua.movievieweryondu.ui.main.MainMvpPresenter;
import com.customproject.chua.movievieweryondu.ui.main.MainMvpView;
import com.customproject.chua.movievieweryondu.ui.main.MainPresenter;
import com.customproject.chua.movievieweryondu.di.ActivityContext;
import com.customproject.chua.movievieweryondu.di.PerActivity;
import com.customproject.chua.movievieweryondu.ui.seatmap.SeatMapMvpPresenter;
import com.customproject.chua.movievieweryondu.ui.seatmap.SeatMapMvpView;
import com.customproject.chua.movievieweryondu.ui.seatmap.SeatMapPresenter;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {

    private AppCompatActivity appCompatActivity;

    public ActivityModule(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return appCompatActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return appCompatActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(MainPresenter<MainMvpView> mainMvpViewMainPresenter) {
        return mainMvpViewMainPresenter;
    }

    @Provides
    @PerActivity
    SeatMapMvpPresenter<SeatMapMvpView> provideSeatMapPresenter(SeatMapPresenter<SeatMapMvpView> seatMapMvpViewSeatMapPresenter) {
        return seatMapMvpViewSeatMapPresenter;
    }


}
