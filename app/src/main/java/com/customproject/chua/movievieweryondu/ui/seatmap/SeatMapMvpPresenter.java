package com.customproject.chua.movievieweryondu.ui.seatmap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.customproject.chua.movievieweryondu.di.PerActivity;
import com.customproject.chua.movievieweryondu.model.CinemasChild;
import com.customproject.chua.movievieweryondu.model.CinemasParent;
import com.customproject.chua.movievieweryondu.model.Dates;
import com.customproject.chua.movievieweryondu.model.Schedule;
import com.customproject.chua.movievieweryondu.model.Seat;
import com.customproject.chua.movievieweryondu.model.Seatmap;
import com.customproject.chua.movievieweryondu.model.TimesChild;
import com.customproject.chua.movievieweryondu.model.TimesParent;
import com.customproject.chua.movievieweryondu.ui.base.MvpPresenter;

import java.util.ArrayList;

@PerActivity
public interface SeatMapMvpPresenter<V extends SeatMapMvpView> extends MvpPresenter<V> {

    void fetchSchedule();

    ArrayList<String> setScheduleDates(Schedule schedule);

    ArrayList<String> setScheduleCinemas(Dates selectedDate, ArrayList<CinemasParent> cinemasParents);

    ArrayList<CinemasChild> getCinemasChildren(Dates selectedDate, ArrayList<CinemasParent> cinemasParents);

    CinemasChild getSelectedCinema(Dates selectedDate, ArrayList<CinemasParent> cinemasParents, int position);

    ArrayList<TimesChild> getTimesChildren(CinemasChild selectedCinema, ArrayList<TimesParent> timesParents);

    ArrayList<String> setScheduleTime(CinemasChild selectedCinema, ArrayList<TimesParent> timesParents);

    TimesChild getSelectedTime(CinemasChild selectedCinema, ArrayList<TimesParent> timesParents, int position);



    void fetchSeatmap();

    ArrayList<Seat> createSeatView(LayoutInflater inflater, ViewGroup seatLayout, Seatmap seatmap, Context context, Seat.SeatOnClickListener seatOnClickListener);

    ArrayList<Integer> getCheckBoxIds();

    ArrayList<String> getAvailableSeats(Seatmap seatmap);

    ArrayList<ArrayList<String>> getSeatMap(Seatmap seatmap);

    Seat getSeatById(String seat_id, ArrayList<Seat> seats);

    ArrayList<String> getSelectedSeats(ArrayList<String> selectedSeats, String seat_id, Boolean isChecked, ArrayList<Seat> seats);

    String computePrice(TimesChild timesChild, ArrayList<String> selectedSeats);

    void groupSelectedSeats(LayoutInflater inflater, ViewGroup selectedSeatsViewGroup, ArrayList<String> selectedSeats);

}
