package com.customproject.chua.movievieweryondu.ui.base;

import com.customproject.chua.movievieweryondu.data.DataManager;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private final CompositeDisposable compositeDisposable;
    private final DataManager dataManager;

    V mvpView;

    @Inject
    public BasePresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        this.dataManager = dataManager;
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public void onAttach(V mvpView) {

        this.mvpView = mvpView;

    }

    @Override
    public void onDettach() {
        compositeDisposable.dispose();
        mvpView = null;
    }

    public V getMvpView() {
        return mvpView;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }
}
