package com.customproject.chua.movievieweryondu.data.network;


import com.customproject.chua.movievieweryondu.AppConstants;
import com.customproject.chua.movievieweryondu.model.Movie;
import com.customproject.chua.movievieweryondu.model.Schedule;
import com.customproject.chua.movievieweryondu.model.Seatmap;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppApiHelper implements ApiHelper {

    @Inject
    public AppApiHelper() {
    }

    @Override
    public Single<Movie> callMovieApi() {

        return Rx2AndroidNetworking.get(AppConstants.movieJson)
                .build()
                .getObjectSingle(Movie.class);

    }

    @Override
    public Single<Schedule> callScheduleApi() {
        return Rx2AndroidNetworking.get(AppConstants.scheduleJson)
                .build()
                .getObjectSingle(Schedule.class);
    }

    @Override
    public Single<Seatmap> callSeatmapApi() {
        return Rx2AndroidNetworking.get(AppConstants.seatmapJson)
                .build()
                .getObjectSingle(Seatmap.class);
    }
}
