package com.customproject.chua.movievieweryondu.model;

import java.util.ArrayList;

public class CinemasParent {

    private String parent;
    private ArrayList<CinemasChild> cinemas;

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public ArrayList<CinemasChild> getCinemas() {
        return cinemas;
    }

    public void setCinemas(ArrayList<CinemasChild> cinemas) {
        this.cinemas = cinemas;
    }
}
