package com.customproject.chua.movievieweryondu.model;

import android.content.Context;
import android.graphics.Color;
import android.provider.CalendarContract;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.customproject.chua.movievieweryondu.R;

public class Seat {

    public enum SeatStatus {

        EMPTY,
        AVAILABLE,
        RESERVED,
        SELECTED
    }

    CheckBox checkBox;
    private String seat_id;
    private String status;

    SeatOnClickListener seatOnClickListener;

    Context context;

    public Seat(CheckBox checkBox, final String seat_id, final String status, Context context, final SeatOnClickListener seatOnClickListener) {
        this.checkBox = checkBox;
        this.seat_id = seat_id;
        this.status = status;
        this.context = context;
        this.seatOnClickListener = seatOnClickListener;

        //checkBox.setTextAppearance(this, R.style.MyCheckbox);



        if (status.equals(SeatStatus.EMPTY.toString()))
            checkBox.setVisibility(View.INVISIBLE);
        else if (status.equals(SeatStatus.RESERVED.toString())) {
            checkBox.setEnabled(false);
            //checkBox.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            //checkBox.setButtonTintList(ContextCompat.getColorStateList(context, R.color.colorPrimary));

        } else if (status.equals(SeatStatus.AVAILABLE.toString())) {
            //checkBox.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGray));
            //checkBox.setButtonTintList(ContextCompat.getColorStateList(context, R.color.colorGray));

        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (isChecked)
                    setStatus(SeatStatus.SELECTED.toString());
                else
                    setStatus(SeatStatus.AVAILABLE.toString());

                seatOnClickListener.seatClickedListener(getSeat_id(), getStatus(), isChecked);

            }
        });

    }

    public String getSeat_id() {
        return seat_id;
    }

    public void setSeat_id(String seat_id) {
        this.seat_id = seat_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }

    public void setCheckBox(CheckBox checkBox) {
        this.checkBox = checkBox;
    }

    public interface SeatOnClickListener {
        void seatClickedListener(String seat_id, String status, Boolean isChecked);
    }

}
