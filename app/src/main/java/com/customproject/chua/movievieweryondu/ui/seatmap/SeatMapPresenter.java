package com.customproject.chua.movievieweryondu.ui.seatmap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.customproject.chua.movievieweryondu.R;
import com.customproject.chua.movievieweryondu.data.DataManager;
import com.customproject.chua.movievieweryondu.model.CinemasChild;
import com.customproject.chua.movievieweryondu.model.CinemasParent;
import com.customproject.chua.movievieweryondu.model.Dates;
import com.customproject.chua.movievieweryondu.model.Schedule;
import com.customproject.chua.movievieweryondu.model.Seat;
import com.customproject.chua.movievieweryondu.model.Seatmap;
import com.customproject.chua.movievieweryondu.model.TimesChild;
import com.customproject.chua.movievieweryondu.model.TimesParent;
import com.customproject.chua.movievieweryondu.ui.base.BasePresenter;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SeatMapPresenter<V extends SeatMapMvpView> extends BasePresenter<V> implements SeatMapMvpPresenter<V> {

    @Inject
    public SeatMapPresenter(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
        fetchSchedule();
        fetchSeatmap();
    }

    @Override
    public void fetchSchedule() {
        getCompositeDisposable().add(getDataManager().callScheduleApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Schedule>() {
                    @Override
                    public void accept(Schedule schedule) throws Exception {

                        getMvpView().showScheduleDates(setScheduleDates(schedule), schedule);

                        getMvpView().showTheater();

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e(SeatMapPresenter.class.getSimpleName(), throwable.getMessage());
                    }
                })
        );
    }

    @Override
    public ArrayList<String> setScheduleDates(Schedule schedule) {

        ArrayList<String> scheduleDatesArray = new ArrayList<>();

        scheduleDatesArray.clear();
        for (Dates d : schedule.getDates())
            scheduleDatesArray.add(d.getLabel());

        return scheduleDatesArray;
    }

    @Override
    public ArrayList<String> setScheduleCinemas(Dates selectedDate, ArrayList<CinemasParent> cinemasParents) {

        ArrayList<String> scheduleCinemasArray = new ArrayList<>();

        scheduleCinemasArray.clear();
        for (CinemasChild child : getCinemasChildren(selectedDate, cinemasParents))
            scheduleCinemasArray.add(child.getLabel());

        return scheduleCinemasArray;
    }

    @Override
    public ArrayList<CinemasChild> getCinemasChildren(Dates selectedDate, ArrayList<CinemasParent> cinemasParents) {

        ArrayList<CinemasChild> cinemasChildren = new ArrayList<>();

        cinemasChildren.clear();
        for (CinemasParent cinema : cinemasParents)
            if (cinema.getParent().equals(selectedDate.getId()))
                cinemasChildren.addAll(cinema.getCinemas());

        return cinemasChildren;
    }

    @Override
    public CinemasChild getSelectedCinema(Dates selectedDate, ArrayList<CinemasParent> cinemasParents, int position) {
        return getCinemasChildren(selectedDate, cinemasParents).get(position);
    }

    @Override
    public ArrayList<String> setScheduleTime(CinemasChild selectedCinema, ArrayList<TimesParent> timesParents) {

        ArrayList<String> scheduleTimesChildArray = new ArrayList<>();

        scheduleTimesChildArray.clear();
        for(TimesChild timesChild: getTimesChildren(selectedCinema, timesParents))
            scheduleTimesChildArray.add(timesChild.getLabel());

        return scheduleTimesChildArray;

    }

    @Override
    public ArrayList<TimesChild> getTimesChildren(CinemasChild selectedCinema, ArrayList<TimesParent> timesParents) {

        ArrayList<TimesChild> timesChildren = new ArrayList<>();

        timesChildren.clear();
        for (TimesParent times: timesParents)
            if (times.getParent().equals(selectedCinema.getId()))
                timesChildren.addAll(times.getTimes());

        return timesChildren;

    }

    @Override
    public TimesChild getSelectedTime(CinemasChild selectedCinema, ArrayList<TimesParent> timesParents, int position) {
        return getTimesChildren(selectedCinema, timesParents).get(position);
    }

    @Override
    public void fetchSeatmap() {
        getCompositeDisposable().add(getDataManager().callSeatmapApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Seatmap>() {
                    @Override
                    public void accept(Seatmap seatmap) throws Exception {
                        getMvpView().display(seatmap);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e(SeatMapPresenter.class.getSimpleName(), throwable.getMessage());
                    }
                })
        );
    }


    @Override
    public ArrayList<String> getAvailableSeats(Seatmap seatmap) {
        return seatmap.getAvailable().getSeats();
    }

    @Override
    public ArrayList<ArrayList<String>> getSeatMap(Seatmap seatmap) {
        return seatmap.getSeatmap();
    }

    @Override
    public ArrayList<Seat> createSeatView(LayoutInflater inflater, ViewGroup seatLayout, Seatmap seatmap, Context context, Seat.SeatOnClickListener seatOnClickListener) {

        ArrayList<Seat> seats = new ArrayList<>();

        for (int i = 0; i < getSeatMap(seatmap).size(); i++) {
            seats.addAll(createSeatXView(inflater, seatLayout, getSeatMap(seatmap).get(i), seatmap, context, seatOnClickListener));
        }

        return seats;

    }

    public ArrayList<Seat> createSeatXView(LayoutInflater inflater, ViewGroup seatLayout, ArrayList<String> seatMapX,
                                           Seatmap seatmap, Context context, Seat.SeatOnClickListener seatOnClickListener) {

        ArrayList<Seat> seats = new ArrayList<>();

        View v = inflater.inflate(R.layout.fullx_seat, null);

        TextView start = v.findViewById(R.id.letter_start);
        TextView end = v.findViewById(R.id.letter_end);

        start.setText(String.valueOf(seatMapX.get(0).charAt(0)));
        end.setText(String.valueOf(seatMapX.get(0).charAt(0)));

        for (int i = 0; i < getCheckBoxIds().size(); i++) {

            CheckBox checkBox = v.findViewById(getCheckBoxIds().get(i));
            String status;

            if (seatMapX.get(i).contains("("))
                status = Seat.SeatStatus.EMPTY.toString();
            else if (getAvailableSeats(seatmap).contains(seatMapX.get(i))) {
                status = Seat.SeatStatus.AVAILABLE.toString();
            } else {
                status = Seat.SeatStatus.RESERVED.toString();
            }

            seats.add(new Seat(checkBox, seatMapX.get(i), status, context, seatOnClickListener));
        }

        seatLayout.addView(v);

        return seats;

    }

    @Override
    public ArrayList<Integer> getCheckBoxIds() {

        ArrayList<Integer> cbXIDs = new ArrayList<>();

        cbXIDs.add(R.id.cb0);
        cbXIDs.add(R.id.cb1);
        cbXIDs.add(R.id.cb2);
        cbXIDs.add(R.id.cb3);
        cbXIDs.add(R.id.cb4);
        cbXIDs.add(R.id.cb5);
        cbXIDs.add(R.id.cb6);
        cbXIDs.add(R.id.cb7);
        cbXIDs.add(R.id.cb8);
        cbXIDs.add(R.id.cb9);
        cbXIDs.add(R.id.cb10);
        cbXIDs.add(R.id.cb11);
        cbXIDs.add(R.id.cb12);
        cbXIDs.add(R.id.cb13);
        cbXIDs.add(R.id.cb14);
        cbXIDs.add(R.id.cb15);
        cbXIDs.add(R.id.cb16);
        cbXIDs.add(R.id.cb17);
        cbXIDs.add(R.id.cb18);
        cbXIDs.add(R.id.cb19);
        cbXIDs.add(R.id.cb20);
        cbXIDs.add(R.id.cb21);
        cbXIDs.add(R.id.cb22);
        cbXIDs.add(R.id.cb23);
        cbXIDs.add(R.id.cb24);
        cbXIDs.add(R.id.cb25);
        cbXIDs.add(R.id.cb26);
        cbXIDs.add(R.id.cb27);
        cbXIDs.add(R.id.cb28);
        cbXIDs.add(R.id.cb29);
        cbXIDs.add(R.id.cb30);
        cbXIDs.add(R.id.cb31);
        cbXIDs.add(R.id.cb32);
        cbXIDs.add(R.id.cb33);
        cbXIDs.add(R.id.cb34);
        cbXIDs.add(R.id.cb35);

        return cbXIDs;
    }

    @Override
    public Seat getSeatById(String seat_id, ArrayList<Seat> seats) {

        for (Seat s : seats)
            if (s.getSeat_id().equals(seat_id))
                return s;

        return null;

    }

    @Override
    public ArrayList<String> getSelectedSeats(ArrayList<String> selectedSeats, String seat_id, Boolean isChecked, ArrayList<Seat> seats) {

        if (selectedSeats.size() < 10) {

            if (isChecked) {
                selectedSeats.add(seat_id);
            } else {
                selectedSeats.remove(seat_id);
            }
        } else {
            getSeatById(seat_id, seats).getCheckBox().setChecked(false);
            selectedSeats.remove(seat_id);
        }

        return selectedSeats;

    }

    @Override
    public String computePrice(TimesChild timesChild, ArrayList<String> selectedSeats) {

        String price = "0";

        try {
            price = timesChild.getPrice();
        }catch (NullPointerException e) {
            e.printStackTrace();
        }

        double price_int = 0;

        try {
            price_int = Double.parseDouble(price);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        double selectedSeatsSize = (double) selectedSeats.size();

        double total = selectedSeatsSize * price_int;

        DecimalFormat formatter = new DecimalFormat("#,###.00");


        String formattedPrice = formatter.format(total);

        return formattedPrice;

    }

    public void groupSelectedSeats(LayoutInflater inflater, ViewGroup selectedSeatsViewGroup, ArrayList<String> selectedSeats) {

        selectedSeatsViewGroup.removeAllViews();

        for(int i = 0; i < selectedSeats.size(); i++) {

            View view = inflater.inflate(R.layout.selected_seat, selectedSeatsViewGroup, false);
            ((TextView) view).setText(selectedSeats.get(i));
            selectedSeatsViewGroup.addView(view);

        }
    }


}
